using Game.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackMelee2Character : StateMachineBehaviour
{
    public bool hasAttacked;
    public bool isCombo;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        hasAttacked = false;
        isCombo = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (hasAttacked) return;

        if (PlayerController.instance.playerAttacking.GetIsAttacking() && PlayerController.instance.playerAttacking.GetIsCombo())
        {
            PlayerController.instance.playerAnimate.SetAnimationMeleeAttack1();
            isCombo = true;
            hasAttacked = true;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PlayerController.instance.playerAttacking.SetIsCombo(false);
        PlayerController.instance.playerAttacking.SetIsAttacking(false);

        PlayerController.instance.playerAttacking.SetHasAttacked(isCombo);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
