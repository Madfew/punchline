using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalData : MonoBehaviour
{
    public static GlobalData Instance;

    private int indexPlayer = 0;

    void Awake() {
        if (Instance == null) {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        } else if (Instance != this) {
            Destroy(gameObject);
        }
    }

    public void ResetData() {
        PlayerPrefs.SetInt("Player", 0);
        LoadData();
        SaveData();
    }


    public void SaveData() {
        PlayerPrefs.SetInt("Player", indexPlayer);
    }

    public void LoadData() {
        indexPlayer = PlayerPrefs.GetInt("Player");
    }

    public void SetIndexPlayer(int value) {
        indexPlayer = value;
        SaveData();
    }

    public int GetIndexPlayer() {
        LoadData();
        return indexPlayer;
    }
}
