using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Manager {
    public class LevelManager : MonoBehaviour {
        public void LoadLevel(string scenename) {
            SceneManager.LoadScene(scenename);
        }
    }
}

