using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI {
    public class Panel : MonoBehaviour {

        [SerializeField] private GameObject root;

        public UnityEvent onOpen;
        public UnityEvent onClose;

        private void Start()
        {
            root.SetActive(false);
        }

        public void SetActive(bool value) {
            root.SetActive(value);

            if(value)
            {
                onOpen?.Invoke();
            }
            else
            {
                onClose?.Invoke();
            }
        }
    }
}

