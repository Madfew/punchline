using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StageAnimationsEvents : MonoBehaviour {

    private Animator animator;

    public UnityEvent onStart;
    public UnityEvent onFinish;

    void Awake() {
        animator = GetComponent<Animator>();
    }

    public void OnStart() {
        onStart?.Invoke();
    }

    public void OnFinish() {
        onFinish?.Invoke();
    }

    public void Play() {
        animator.SetTrigger("play");
    }
}
