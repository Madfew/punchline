using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Abyss.Game {
    public class TriggerEvent : MonoBehaviour {

        public UnityEvent onInteract;

        private bool isActive;

        private void OnTriggerEnter2D(Collider2D collision) {
            if (isActive) return;

            if (collision.gameObject.CompareTag("Player")) {
                onInteract?.Invoke();
                isActive = true;
            }
        }
    }
}

