using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Player {
    public class Animate : MonoBehaviour {

        private Movement movement;
        private Blocking blocking;
        private Animator anim;
        //private SpriteRenderer spriteRenderer;

        private bool isFacingRight;

        private void Awake() {
            //spriteRenderer = GetComponent<SpriteRenderer>();
            movement = GetComponent<Movement>();
            blocking = GetComponent<Blocking>();
            anim = GetComponent<Animator>();
        }

        // Start is called before the first frame update
        void Start() {
            isFacingRight = true;
        }

        // Update is called once per frame
        void Update() {
            anim.SetFloat("Horizontal", Mathf.Abs(movement.GetDirection.x));

            if(!isFacingRight && movement.GetDirection.x > 0f) {
                Flip();
            } else if (isFacingRight && movement.GetDirection.x < 0f){
                Flip();
            }

            anim.SetBool("IsGrounded", movement.GetIsGrounded());
            anim.SetBool("IsBlocking", blocking.GetIsBlocking());
        }

        public void Flip() {
            isFacingRight = !isFacingRight;
            transform.Rotate(0f, 180f, 0f);
            //Vector3 scale = transform.localScale;
            //scale.x *= -1;
            //transform.localScale = scale;
            //transform.localScale = new Vector3(-1, 1, 1);
            //spriteRenderer.flipX = !isFacingRight;
        }

        public void SetAnimationJump() {
            anim.Play("JumpStart_Jorge");
        }

        public void SetAnimationMeleeAttack1() {
            anim.Play("Attack1_Jorge");
        }

        public void SetAnimationMeleeAttack2() {
            anim.Play("Attack2_Jorge");
        }

        public void SetAnimationDistanceAttack() {
            anim.Play("Attack_Distance_Jorge");
        }

        public void SetAnimationBlock() {
            anim.Play("Block_Jorge");
        }
    }
}

