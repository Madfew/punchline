using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using DG.Tweening;

namespace Game.Player {
    public class Attacking : MonoBehaviour {

        [SerializeField] private float attackPower;

        [SerializeField] private GameObject attackPrefab;
        [SerializeField] private Transform attackPosition;

        private bool canAttack;
        private bool isAttacking;
        private bool hasAttacked;
        private bool isCombo;
        private bool isRight;

        private bool cooldownSecondAttack;
        private float timerSecondAttack = 3f;

        private bool hasDistanceAttack;

        private Rigidbody2D rb;
        private Movement movement;

        public UnityEvent onMeleeAttack1;
        public UnityEvent onMeleeAttack2;
        public UnityEvent onDistanceAttack;

        private void Awake() {
            rb = GetComponent<Rigidbody2D>();
            movement = GetComponent<Movement>();
        }

        void Start() {
            isRight = true;
            canAttack = true;
        }

        void Update() {
            if (movement.GetDirection.x > 0f) {
                isRight = true;
            }
            else if (movement.GetDirection.x < 0f) {
                isRight = false;
            }

            if (cooldownSecondAttack) {
                timerSecondAttack -= Time.deltaTime;
                if(timerSecondAttack < 0) {
                    cooldownSecondAttack = false;
                    timerSecondAttack = 3f;
                }
            }
        }

        public void MeleeAttack(InputAction.CallbackContext context) {
            if (!canAttack || !Rules.Instance.CanAttack()) return;

            if (context.performed && !isAttacking) {
                DOVirtual.Float(45, 0, 0.45f, RigidbodyDrag);

                isAttacking = true;
                hasAttacked = true;

                if (isRight) {
                    rb.AddForce(Vector2.right * attackPower, ForceMode2D.Impulse);
                }
                else {
                    rb.AddForce(Vector2.left * attackPower, ForceMode2D.Impulse);
                }
                //Debug.Log("Attack_1");
            }
        }

        public void DistanceAttack(InputAction.CallbackContext context) {
            if (!canAttack || cooldownSecondAttack || !Rules.Instance.CanAttackDistance()) return;

            if (context.performed) {

                hasDistanceAttack = true;
                cooldownSecondAttack = true;
                onDistanceAttack?.Invoke();

                //Instantiate(attackPrefab, attackPosition.position, attackPosition.rotation);
                //Debug.Log("Attack_2");
            }
        }

        void RigidbodyDrag(float x){
            rb.drag = x;
        }

        public void GenerateDistanceAttack() {
            hasDistanceAttack = false;
            Instantiate(attackPrefab, attackPosition.position, attackPosition.rotation);
        }

        public void SetIsAttacking(bool value){
            isAttacking = value;
        }

        public bool GetIsAttacking() {
            return isAttacking;
        }

        public void SetIsCombo(bool value) {
            isCombo = value;
        }

        public bool GetIsCombo() {
            return isCombo;
        }

        public void SetHasAttacked(bool value) {
            hasAttacked = value;
        }

        public bool GetHasAttacked() {
            return hasAttacked;
        }

        public bool GetHasDistanceAttack() {
            return hasDistanceAttack;
        }
    }
}

