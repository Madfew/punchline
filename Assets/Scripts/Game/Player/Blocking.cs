using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Blocking : MonoBehaviour
{
    public UnityEvent onBlocking;
    public UnityEvent onStopBlocking;

    [SerializeField] private bool isBlocking;

    public void Block(InputAction.CallbackContext context) {
        if (context.performed) {
            onBlocking?.Invoke();
            isBlocking = true;
        }

        if (context.canceled) {
            isBlocking = false;
            onStopBlocking?.Invoke();
        }
    }

    public bool GetIsBlocking() {
        return isBlocking; 
    }
}
