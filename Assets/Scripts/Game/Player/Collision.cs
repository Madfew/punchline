using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private float KNOCKBACK_POWER;
    [SerializeField] private float KNOCKBACK_COOLDOWN;

    private Rigidbody2D rb;
    private CapsuleCollider2D coll;

    private void Awake() {
        rb = GetComponentInParent<Rigidbody2D>();
        coll = GetComponent<CapsuleCollider2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Attack1")) {
            //isDamaged = true;

            //onKnockbackStart?.Invoke();

            //GetComponent<CapsuleCollider2D>().isTrigger = false;
            float posX = player.transform.position.x - collision.transform.position.x;
            Vector3 direction = new Vector3(posX, 1, 0);
            rb.AddForce(direction * KNOCKBACK_POWER, ForceMode2D.Impulse);

            //DOVirtual.DelayedCall(KNOCKBACK_COOLDOWN, FinishKnockback);
        }
    }

}
