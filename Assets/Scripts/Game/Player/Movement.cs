using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

namespace Game.Player {
    public class Movement : MonoBehaviour {

        [SerializeField] private float speed;
        [SerializeField] private float jumpHeight;
        [SerializeField] private float defaultGravity;
        [SerializeField] private float fallGravity;
        [SerializeField] private Transform groundCheck;
        [SerializeField] private LayerMask groundLayer;

        private Rigidbody2D rb;

        private float horizontalInput;
        private bool hasJumped;
        private bool isGrounded;
        private bool canMove;
        private bool isAttack;

        private float posX;
        private float timer;

        private Vector2 moveDirection;

        public UnityEvent onJump;

        private void Awake() {
            rb = GetComponent<Rigidbody2D>();
        }

        // Start is called before the first frame update
        void Start() {
            canMove = true;
            isGrounded = true;
        }

        private void Update() {
            //rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);

            if(isAttack) {
                horizontalInput = 0f;
                timer -= Time.deltaTime;
                if(timer < 0f) {
                    isAttack = false;
                    horizontalInput = posX;
                }
            }

            if (rb.velocity.y >= 0) {
                rb.gravityScale = defaultGravity;
            }
            else if (rb.velocity.y < 0) {
                isGrounded = IsGrounded();
                rb.gravityScale = fallGravity;
            }
        }

        private void FixedUpdate() {
            if (isAttack) return;

            moveDirection = new Vector2(horizontalInput, 0);
            //rb.velocity = moveDirection * moveSpeed * speed;
            transform.Translate(moveDirection * speed * Time.fixedDeltaTime);
        }

        private bool IsGrounded() {
            return Physics2D.OverlapCircle(groundCheck.position, 0.25f, groundLayer);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(groundCheck.position, 0.25f);
        }

        public void Move(InputAction.CallbackContext context) {
            if (!canMove || !Rules.Instance.CanMove()) {
                if (context.canceled) {
                    posX = 0;
                }
                return; 
            }

            posX = context.ReadValue<Vector2>().x;
            horizontalInput = posX;

            //Debug.Log(context.ReadValue<Vector2>().x);
            if(context.ReadValue<Vector2>().x == -1) {
                speed = -7;
            } else if (context.ReadValue<Vector2>().x == 1) {
                speed = Mathf.Abs(speed);
            }
        }

        public void Jump(InputAction.CallbackContext context) {
            if (!canMove || !Rules.Instance.CanJump()) return;

            if (context.performed && IsGrounded()) {
                onJump?.Invoke();

                hasJumped = true;
                isGrounded = false;

                rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
            }
        }

        public void SetMoveZero() {
            timer = 0.45f;
            isAttack = true;
        }

        public void SetTotalZero() {
            horizontalInput = 0f;
        }

        public void ReturnMovement() {
            horizontalInput = posX;
        }

        public Vector2 GetDirection => moveDirection;

        public bool GetHasJumped => hasJumped;

        public bool GetIsGrounded() {
            return isGrounded;
        }
    }
}

