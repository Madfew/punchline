using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Player {
    public class PlayerController : MonoBehaviour {

        public static PlayerController instance;

        public Movement[] playersMovement;
        public Attacking[] playersAttacking;
        public Animate[] playersAnimate;

        public Movement playerMovement;
        public Attacking playerAttacking;
        public Animate playerAnimate;

        public bool hasAttacked;

        [SerializeField] private GameObject[] players;
        private int currentPlayer;

        private void Awake() {
            instance = this;
        }

        void Start() {
            foreach(GameObject go in players) {
                go.SetActive(false);
            }

            currentPlayer = GlobalData.Instance.GetIndexPlayer();
            players[currentPlayer].SetActive(true);

            playerMovement = playersMovement[currentPlayer];
            playerAttacking = playersAttacking[currentPlayer];
            playerAnimate = playersAnimate[currentPlayer];
        }

    }
}

