using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Player;

public class Rules : MonoBehaviour {

    static public Rules Instance;

    [SerializeField] Movement[] movement;
    [SerializeField] Attacking[] attacking;
    [SerializeField] Blocking[] blocking;

    private int currentPlayer;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        currentPlayer = GlobalData.Instance.GetIndexPlayer();
    }

    public bool CanMove() {
        if (blocking[currentPlayer].GetIsBlocking() || attacking[currentPlayer].GetHasDistanceAttack()) return false;
        return true;
    }

    public bool CanJump() {
        if (attacking[currentPlayer].GetHasAttacked() || blocking[currentPlayer].GetIsBlocking()) return false;

        return true;
    }

    public bool CanAttack() {
        if (movement[currentPlayer].GetIsGrounded() || blocking[currentPlayer].GetIsBlocking()) return true;

        return false;
    }

    public bool CanAttackDistance() {
        if (blocking[currentPlayer].GetIsBlocking()) return false;

        return true;
    }
}
