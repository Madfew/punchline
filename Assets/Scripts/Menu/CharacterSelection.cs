using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI {
    public class CharacterSelection : MonoBehaviour {

        public GameObject[] characters;

        public UnityEvent onSelected;

        private bool trigger;

        void Start() {
            foreach (GameObject character in characters) {
                character.SetActive(false);
            }
        }

        public void SetCharacter(int value) {
            if (!trigger) {
                trigger = true;
                onSelected?.Invoke();
            }

            foreach (GameObject character in characters) {
                character.SetActive(false);
            }

            characters[value].gameObject.SetActive(true);
        }
    }
}

