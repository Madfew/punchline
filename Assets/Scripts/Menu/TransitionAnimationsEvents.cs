using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.UI {
    public class TransitionAnimationsEvents : MonoBehaviour {

        private Animator animator;

        public UnityEvent onStart;
        public UnityEvent onFinish;

        public UnityEvent onTransitionLeft;
        public UnityEvent onTransitionRight;

        void Awake() {
            animator = GetComponent<Animator>();
        }

        public void OnStart() {
            onStart?.Invoke();
        }

        public void OnFinish() {
            onFinish?.Invoke();
        }

        public void OnTransitionLeft() {
            onTransitionLeft?.Invoke();
        }

        public void OnTransitionRight() {
            onTransitionRight?.Invoke();
        }

        public void PlayLeft() {
            animator.SetTrigger("left");
        }

        public void PlayRight() {
            animator.SetTrigger("right");
        }
    }
}

